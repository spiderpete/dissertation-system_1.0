#!/bin/bash

# Prepare data for DNN code with head motion prediction
#
#
#

## Set this to point to SPTK's x2x (this is used to convert the data to binary format)
X2X=/afs/inf.ed.ac.uk/group/cstr/projects/galatea/tools/SPTK-3.7/bin/x2x
TOPDATADIR=/afs/inf.ed.ac.uk/group/cstr/projects/galatea/PeterS/dnn_top/dnn_peter/data
HEADDIR=$TOPDATADIR/original/
MFCCDIR=$TOPDATADIR/original/
SPEAKER=Adam
TYPE=i

mkdir -p $TOPDATADIR/dnn/$SPEAKER
mkdir -p $TOPDATADIR/dnn/$SPEAKER/mfcc
mkdir -p $TOPDATADIR/dnn/$SPEAKER/head


## Prepare feature files. Use 2 files for training, 1 for validation, 1 for testing

sed '1,30744!d' $TOPDATADIR/original/$SPEAKER/mfcc/${SPEAKER}_03_${TYPE}.mfcc+delta |  tr '\t' '\n'  | $X2X +af >  $TOPDATADIR/dnn/$SPEAKER/mfcc/${SPEAKER}_03_${TYPE}.mfcc
sed '1,30421!d' $TOPDATADIR/original/$SPEAKER/mfcc/${SPEAKER}_05_${TYPE}.mfcc+delta  |  tr '\t' '\n'  | $X2X +af >  $TOPDATADIR/dnn/$SPEAKER/mfcc/${SPEAKER}_05_${TYPE}.mfcc
sed '1,31596!d' $TOPDATADIR/original/$SPEAKER/mfcc/${SPEAKER}_08_${TYPE}.mfcc+delta  |  tr '\t' '\n'  | $X2X +af >  $TOPDATADIR/dnn/$SPEAKER/mfcc/${SPEAKER}_08_${TYPE}.mfcc
sed '1,30298!d' $TOPDATADIR/original/$SPEAKER/mfcc/${SPEAKER}_10_${TYPE}.mfcc+delta  |  tr '\t' '\n'  | $X2X +af >  $TOPDATADIR/dnn/$SPEAKER/mfcc/${SPEAKER}_10_${TYPE}.mfcc


## Prepare head motion files (same length as feature files!)

sed '1,30744!d' $TOPDATADIR/original/$SPEAKER/head/${SPEAKER}_03_${TYPE}.ea132+delta2 |  tr '\t' '\n'  | $X2X +af >  $TOPDATADIR/dnn/$SPEAKER/head/${SPEAKER}_03_${TYPE}.head
sed '1,30421!d' $TOPDATADIR/original/$SPEAKER/head/${SPEAKER}_05_${TYPE}.ea132+delta2 |  tr '\t' '\n'  | $X2X +af >  $TOPDATADIR/dnn/$SPEAKER/head/${SPEAKER}_05_${TYPE}.head
sed '1,31596!d' $TOPDATADIR/original/$SPEAKER/head/${SPEAKER}_08_${TYPE}.ea132+delta2 |  tr '\t' '\n'  | $X2X +af >  $TOPDATADIR/dnn/$SPEAKER/head/${SPEAKER}_08_${TYPE}.head
sed '1,30298!d' $TOPDATADIR/original/$SPEAKER/head/${SPEAKER}_10_${TYPE}.ea132+delta2 |  tr '\t' '\n'  | $X2X +af >  $TOPDATADIR/dnn/$SPEAKER/head/${SPEAKER}_10_${TYPE}.head


## make list of training data utterances:

echo ${SPEAKER}_03_${TYPE} > $TOPDATADIR/dnn/filelist_${SPEAKER}.txt
echo ${SPEAKER}_05_${TYPE} >> $TOPDATADIR/dnn/filelist_${SPEAKER}.txt
echo ${SPEAKER}_08_${TYPE} >> $TOPDATADIR/dnn/filelist_${SPEAKER}.txt
echo ${SPEAKER}_10_${TYPE} >> $TOPDATADIR/dnn/filelist_${SPEAKER}.txt
