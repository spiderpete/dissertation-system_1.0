
source /afs/inf.ed.ac.uk/group/cstr/projects/galatea/PeterS/python_env/bin/activate

PYTHONPATH=${PYTHONPATH}:/afs/inf.ed.ac.uk/group/project/dnn_tts/tools/site-packages/

gpu_id=$(python ./gpu_lock.py --id-to-hog)

echo $gpu_id

# usage: run_dnn.sh [config file name]


#CONFIG="./configuration/myconfigfile.conf"
CONFIG="/afs/inf.ed.ac.uk/group/cstr/projects/galatea/PeterS/dnn_top/dnn_peter/config/initial.conf"
if [ $# -gt 0 ]; then
	CONFIG=$1
fi


if [ $gpu_id -gt -1 ]; then
    THEANO_FLAGS="cuda.root=/opt/cuda-6.5.19,mode=FAST_RUN,device=gpu$gpu_id,floatX=float32"
    CUDA_VISIBLE_DEVICES="1,2,3,4"
    export THEANO_FLAGS CUDA_VISIBLE_DEVICES
    
    echo $CONFIG
    python  ./run_dnn_backup.py ${CONFIG}

    python ./gpu_lock.py --free $gpu_id
else
    echo 'Let us wait! No GPU is available!'

fi
