###THEANO_FLAGS='cuda.root=/opt/cuda-5.0.35,mode=FAST_RUN,device=gpu0,floatX=float32,exception_verbosity=high' python dnn.py
"""
"""
import cPickle
import os
import sys
import time

import numpy

import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from layers.layers import LinearLayer, SigmoidLayer, HiddenLayer, GeneralLayer
from utils.providers import ListDataProvider

import logging

class MultiStreamDNN(object):
    '''
    multi-stream DNN: each feature has its own stream but without private hidden layer.
                      if you want to each feature to have its own private hidden layer, please refer ms_ph_dnn.py
    '''
    def __init__(self, numpy_rng, n_ins=784, ms_outs=[50, 50], l1_reg = None, l2_reg = None, 
                 hidden_layers_sizes=[500, 500], stream_weights = [1.0, 1.0],
                 hidden_activation='tanh', output_activation='linear', out_activation_list=['linear', 'linear', 'linear', 'linear']):

        logger = logging.getLogger("Multi-stream DNN initialization")

        self.sigmoid_layers = []
        self.params = []
        self.delta_params   = []

        self.final_layers = []
        self.private_params = {}
        self.private_delta_params = {}
        
        self.ms_outs = ms_outs

        self.stream_weights = stream_weights

        self.n_layers = len(hidden_layers_sizes)
        
        self.n_streams = len(ms_outs)
        
        self.output_activation = output_activation

        self.l1_reg = l1_reg
        self.l2_reg = l2_reg

        assert self.n_layers > 0

        # allocate symbolic variables for the data
        self.x = T.matrix('x') 
        self.y = T.matrix('y') 

        for i in xrange(self.n_layers):
            if i == 0:
                input_size = n_ins
            else:
                input_size = hidden_layers_sizes[i - 1]

            if i == 0:
                layer_input = self.x
            else:
                layer_input = self.sigmoid_layers[-1].output

            sigmoid_layer = HiddenLayer(rng=numpy_rng,
                                        input=layer_input,
                                        n_in=input_size,
                                        n_out=hidden_layers_sizes[i],
                                        activation=T.tanh)  ##T.nnet.sigmoid)  # 
            self.sigmoid_layers.append(sigmoid_layer)
            self.params.extend(sigmoid_layer.params)
            self.delta_params.extend(sigmoid_layer.delta_params)

        hidden_output_size = hidden_layers_sizes[-1]
        ## add private hidden layers, and final layer
        for i in xrange(self.n_streams): 
            if not self.private_params.has_key(str(i)):
                self.private_params[str(i)] = []
                self.private_delta_params[str(i)] = []

            final_layer = LinearLayer(rng = numpy_rng,
                                       input = sigmoid_layer.output,
                                       n_in = hidden_output_size,
                                       n_out = ms_outs[i])

            self.final_layers.append(final_layer)
            self.private_params[str(i)].extend(final_layer.params)
            self.private_delta_params[str(i)].extend(final_layer.delta_params)

        ### MSE
        self.finetune_cost_list = []
        self.finetune_cost_all = 0.0

        self.error_list = []
        self.errors = 0.0
        y_start_index = 0

        for i in xrange(self.n_streams):
            finetune_cost   = T.mean(T.sum( (self.final_layers[i].output - self.y[:, y_start_index:y_start_index+ms_outs[i]])**2, axis=1 ))

            ## continue to calculate cost for each stream
            ## each stream can have their own L2 regularisation parameter and even learning rate
            ## for simplify, we use the same paramater for all the streams
            if self.l2_reg is not None:
                W = self.private_params[str(i)][0]
                finetune_cost += self.l2_reg * T.sqr(W).sum()

            self.finetune_cost_all += finetune_cost * self.stream_weights[i] 
            self.finetune_cost_list.append(finetune_cost)

            construct_error = T.mean(T.sum( (self.final_layers[i].output - self.y[:, y_start_index:y_start_index+ms_outs[i]])**2, axis=1 ))
            self.errors += construct_error
            self.error_list.append(construct_error)

            y_start_index += ms_outs[i]

        ### calculate the cost for regularisation 
        ### L1-norm
        if self.l1_reg is not None:
            for i in xrange(self.n_layers):
                W = self.params[i * 2]
                self.finetune_cost_all += self.l1_reg * (abs(W).sum())

        ### L2-norm
        if self.l2_reg is not None:
            for i in xrange(self.n_layers):
                W = self.params[i * 2]
                self.finetune_cost_all += self.l2_reg * T.sqr(W).sum()

    def build_finetune_functions(self, train_shared_xy, valid_shared_xy, batch_size, lr_weights):
        (train_set_x, train_set_y) = train_shared_xy
        (valid_set_x, valid_set_y) = valid_shared_xy

        # compute number of minibatches for training, validation and testing
        n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
        n_valid_batches /= batch_size
        n_train_batches = train_set_x.get_value(borrow=True).shape[0]
        n_train_batches /= batch_size

        index = T.lscalar('index')  # index to a [mini]batch
        learning_rate = T.fscalar('learning_rate')
        momentum = T.fscalar('momentum')

        layer_size = len(self.params)
        lr_list = []
        for i in xrange(layer_size):
            lr_list.append(learning_rate)

        ##top 2 layers use a smaller learning rate
        if layer_size > 2:
            for i in range(layer_size-2, layer_size):
                lr_list[i] = learning_rate * 0.5

        
        # compute the gradients with respect to the common lower layers parameters
        common_gparams = T.grad(self.finetune_cost_all, self.params)

        # compute the gradients for each stream
        private_gparams = {}
        for i in xrange(self.n_streams):
            private_gparams[str(i)] = T.grad(self.finetune_cost_list[i], self.private_params[str(i)])

        updates = theano.compat.python2x.OrderedDict()

        current_layer_index = 0
        for dparam, gparam in zip(self.delta_params, common_gparams):
            updates[dparam] = momentum * dparam - gparam * lr_list[current_layer_index]
            current_layer_index += 1

        for i in xrange(self.n_streams):
            half_learning_rate = learning_rate * lr_weights[i]

            for dparam, gparam in zip(self.private_delta_params[str(i)], private_gparams[str(i)]):
                updates[dparam] = momentum * dparam - gparam * half_learning_rate

        for dparam, param in zip(self.delta_params, self.params):
            updates[param] = param + updates[dparam]

        for i in xrange(self.n_streams):
            for dparam, param in zip(self.private_delta_params[str(i)], self.private_params[str(i)]):
                updates[param] = param + updates[dparam]

        train_fn = theano.function(inputs=[index, theano.Param(learning_rate, default = 0.0001),
              theano.Param(momentum, default = 0.5)],
              outputs=self.errors,
              updates=updates,
              givens={self.x: train_set_x[index * batch_size:
                                          (index + 1) * batch_size],
                      self.y: train_set_y[index * batch_size:
                                          (index + 1) * batch_size]})

        valid_fn = theano.function([index], 
              outputs=self.errors,
              givens={self.x: valid_set_x[index * batch_size:
                                          (index + 1) * batch_size],
                      self.y: valid_set_y[index * batch_size:
                                          (index + 1) * batch_size]})

        valid_score_i = theano.function([index], 
              outputs=self.errors,
              givens={self.x: valid_set_x[index * batch_size:
                                          (index + 1) * batch_size],
                      self.y: valid_set_y[index * batch_size:
                                          (index + 1) * batch_size]})
        # Create a function that scans the entire validation set
        def valid_score():
            return [valid_score_i(i) for i in xrange(n_valid_batches)]

        return train_fn, valid_fn, valid_score

    def parameter_prediction(self, test_set_x):  #, batch_size

#        self.ms_outs = [150, 3, 1, 75]
        n_test_set_x = test_set_x.get_value(borrow=True).shape[0]
        
        n_outs = sum(self.ms_outs)
        predict_parameter = numpy.zeros((n_test_set_x, n_outs))
        
        y_start_index = 0
        for i in xrange(self.n_streams):
            test_out = theano.function([], self.final_layers[i].output,
                                       givens={self.x: test_set_x[0:n_test_set_x]})
            
            predict_parameter[:, y_start_index:y_start_index+self.ms_outs[i]] = test_out()
            
            y_start_index += self.ms_outs[i]
            
        return predict_parameter


if __name__ == '__main__':

    train_scp = '/afs/inf.ed.ac.uk/group/project/dnn_tts/data/nick/nn_scp/train.scp'
    valid_scp = '/afs/inf.ed.ac.uk/group/project/dnn_tts/data/nick/nn_scp/gen.scp'
	
    model_dir = '/afs/inf.ed.ac.uk/group/project/dnn_tts/practice/nnets_model'
	
    log_dir =  '/afs/inf.ed.ac.uk/group/project/dnn_tts/practice/log'   

    finetune_lr=0.01
    pretraining_epochs=100
    pretrain_lr=0.01
    training_epochs=100
    batch_size=32

    n_ins = 898
    n_outs = 229
    
    hidden_layers_sizes = [512, 512, 512]
             
#    test_DBN(train_scp, valid_scp, log_dir, model_dir, n_ins, n_outs, hidden_layers_sizes, 
#             finetune_lr, pretraining_epochs, pretrain_lr, training_epochs, batch_size)

    dnn_generation()

