
#import cPickle

import os
import sys
import time

import numpy

import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from layers.layers import LinearLayer, SigmoidLayer, HiddenLayer
#from mlp import HiddenLayer
from dA import dA   ## the denoise autoencoder is based on the theano tutorial. Will make it more general, such support other activation functions


## note: the activation function for autoencoder and the finetune MLP should be consistent
##       if input data are normalise to [0, 1] sigmoid to be used in the autoencoder.
##       if input data are normalise to [0, 1], tanh cannot be used in both autoencoder and MLP     

class StackedDenoiseAutoEncoder(object):
    def __init__(self, numpy_rng, theano_rng=None, n_ins=784,  n_outs=10, 
                 l1_reg = None, l2_reg = None, 
                 hidden_layers_sizes=[500, 500],
                 corruption_levels=[0.1, 0.1]):

        self.sigmoid_layers = []
        self.dA_layers = []
        self.params = []
        self.delta_params   = []
        self.n_layers = len(hidden_layers_sizes)

        self.l1_reg = l1_reg
        self.l2_reg = l2_reg

        assert self.n_layers > 0

        if not theano_rng:
            theano_rng = RandomStreams(numpy_rng.randint(2 ** 30))

        # allocate symbolic variables for the data
        self.x = T.matrix('x')
        self.y = T.matrix('y')  

        for i in xrange(self.n_layers):
            # construct the sigmoidal layer

            # the size of the input is either the number of hidden units of
            # the layer below or the input size if we are on the first layer
            if i == 0:
                input_size = n_ins
            else:
                input_size = hidden_layers_sizes[i - 1]

            # the input to this layer is either the activation of the hidden
            # layer below or the input of the SdA if you are on the first
            # layer
            if i == 0:
                layer_input = self.x
            else:
                layer_input = self.sigmoid_layers[-1].output

            sigmoid_layer = HiddenLayer(rng=numpy_rng,
                                        input=layer_input,
                                        n_in=input_size,
                                        n_out=hidden_layers_sizes[i],
                                        activation=T.nnet.sigmoid)
            # add the layer to our list of layers
            self.sigmoid_layers.append(sigmoid_layer)
            self.params.extend(sigmoid_layer.params)
            self.delta_params.extend(sigmoid_layer.delta_params)

            # Construct a denoising autoencoder that shared weights with this sigmoid layer
            dA_layer = dA(numpy_rng=numpy_rng,
                          theano_rng=theano_rng,
                          input=layer_input,
                          n_visible=input_size,
                          n_hidden=hidden_layers_sizes[i],
                          W=sigmoid_layer.W,
                          bhid=sigmoid_layer.b)
            self.dA_layers.append(dA_layer)


        self.final_layer = LinearLayer(rng = numpy_rng,
                                       input=self.sigmoid_layers[-1].output,
                                       n_in=hidden_layers_sizes[-1],
                                       n_out=n_outs)

        self.params.extend(self.final_layer.params)
        self.delta_params.extend(self.final_layer.delta_params)

        self.finetune_cost = T.mean(T.sum( (self.final_layer.output-self.y)*(self.final_layer.output-self.y), axis=1 ))
        
        self.errors = T.mean(T.sum( (self.final_layer.output-self.y)*(self.final_layer.output-self.y), axis=1 ))

        ### L1-norm
        if self.l1_reg is not None:
            for i in xrange(self.n_layers):
                W = self.params[i * 2]
                self.finetune_cost += self.l1_reg * (abs(W).sum())

        ### L2-norm
        if self.l2_reg is not None:
            for i in xrange(self.n_layers):
                W = self.params[i * 2]
                self.finetune_cost += self.l2_reg * T.sqr(W).sum()

    def pretraining_functions(self, train_set_x, batch_size):

        # index to a [mini]batch
        index = T.lscalar('index')  # index to a minibatch
        corruption_level = T.scalar('corruption')  # % of corruption to use
        learning_rate = T.scalar('learning_rate')  # learning rate to use

        pretrain_fns = []
        for dA in self.dA_layers:
            # get the cost and the updates list
            cost, updates = dA.get_cost_updates(corruption_level,
                                                learning_rate)
            # compile the theano function
            fn = theano.function(inputs=[index, theano.Param(corruption_level, default=0.2),
                                         theano.Param(learning_rate, default=0.1)],
                                 outputs=cost, updates=updates,
                                 givens={self.x: train_set_x[index*batch_size:(index+1)*batch_size]})

            # append `fn` to the list of functions
            pretrain_fns.append(fn)

        return pretrain_fns

    def build_finetune_functions(self, train_set_xy, valid_set_xy, batch_size):

        (train_set_x, train_set_y) = train_set_xy
        (valid_set_x, valid_set_y) = valid_set_xy

        # compute number of minibatches for training, validation and testing
        n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
        n_valid_batches /= batch_size

        index = T.lscalar('index')  # index to a [mini]batch
        learning_rate = T.fscalar('learning_rate')
        momentum = T.fscalar('momentum')

        layer_size = len(self.params)
        lr_list = []
        for i in xrange(layer_size):
            lr_list.append(learning_rate)

        ##top 2 layers use a smaller learning rate
        if layer_size > 4:
            for i in range(layer_size-4, layer_size):
                lr_list[i] = learning_rate * 0.5

        # compute list of fine-tuning updates
        # compute the gradients with respect to the model parameters
        gparams = T.grad(self.finetune_cost, self.params)

        updates = theano.compat.python2x.OrderedDict()
        layer_index = 0
        for dparam, gparam in zip(self.delta_params, gparams):
            updates[dparam] = momentum * dparam - gparam * lr_list[layer_index]
            layer_index += 1

        for dparam, param in zip(self.delta_params, self.params):
            updates[param] = param + updates[dparam]

        train_fn = theano.function(inputs=[index, theano.Param(learning_rate, default = 0.0001),
                                           theano.Param(momentum, default = 0.5)],
                                   outputs=self.finetune_cost, updates=updates,
                                   givens={self.x: train_set_x[index*batch_size:(index+1)*batch_size],
                                           self.y: train_set_y[index*batch_size:(index+1)*batch_size]},
                                   name='train')

        valid_fn = theano.function([], outputs=self.errors,
                                  givens={self.x: valid_set_x,
                                          self.y: valid_set_y},
                                  name='valid')

        return train_fn, valid_fn

    def parameter_prediction(self, test_set_x):  #, batch_size

        n_test_set_x = test_set_x.get_value(borrow=True).shape[0]

        test_out = theano.function([], self.final_layer.output,
              givens={self.x: test_set_x[0:n_test_set_x]})

        predict_parameter = test_out()

        return predict_parameter
        
    
    ### when using Autoencoder to learning feature, use this function to generate the top hidden layer
    def generate_top_hidden_layer(self, test_set_x):
        
        n_test_set_x = test_set_x.get_value(borrow=True).shape[0]

        test_out = theano.function([], self.sigmoid_layers[-1].output,
              givens={self.x: test_set_x[0:n_test_set_x]})

        predict_parameter = test_out()

        return predict_parameter

    ## when using autoencoder to learning feature, use this function to generate the specific hidden layer
    ## a bottle neck layer can be made by controling hidden_layers_sizes
    def generate_hidden_layer(self, test_set_x, hidden_index):
        
        n_test_set_x = test_set_x.get_value(borrow=True).shape[0]

        test_out = theano.function([], self.sigmoid_layers[hidden_index].output,
              givens={self.x: test_set_x[0:n_test_set_x]})

        predict_parameter = test_out()

        return predict_parameter

#def test_SdA(finetune_lr=0.1, pretraining_epochs=15,
#             pretrain_lr=0.001, training_epochs=1000,
#             dataset='mnist.pkl.gz', batch_size=1):

#    datasets = load_data(dataset)

#    train_set_x, train_set_y = datasets[0]
#    valid_set_x, valid_set_y = datasets[1]


#if __name__ == '__main__':
#    test_SdA()
