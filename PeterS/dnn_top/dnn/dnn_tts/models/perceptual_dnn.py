###THEANO_FLAGS='cuda.root=/opt/cuda-5.0.35,mode=FAST_RUN,device=gpu0,floatX=float32,exception_verbosity=high' python dnn.py
"""
"""
import cPickle
import os
import sys
import time

import numpy, math

import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from layers.layers import LinearLayer, SigmoidLayer, HiddenLayer, GeneralLayer
from utils.providers import ListDataProvider

import logging

class PerceptualDNN(object):
    '''
    multi-stream DNN: each feature has its own stream but without private hidden layer.
                      if you want to each feature to have its own private hidden layer, please refer ms_ph_dnn.py
    '''

    def __init__(self, numpy_rng, theano_rng=None, n_ins=784,
                 n_outs=10, l1_reg = None, l2_reg = None, 
                 hidden_layers_sizes=[500, 500], 
                 hidden_activation='tanh', output_activation='linear'):

        logger = logging.getLogger("DNN initialization")

        self.sigmoid_layers = []
        self.params = []
        self.delta_params   = []
        self.n_layers = len(hidden_layers_sizes)
        
        self.output_activation = output_activation

        self.l1_reg = l1_reg
        self.l2_reg = l2_reg

        assert self.n_layers > 0

        if not theano_rng:
            theano_rng = RandomStreams(numpy_rng.randint(2 ** 30))

        # allocate symbolic variables for the data
        self.x = T.matrix('x') 
        self.y = T.matrix('y') 

        self.percetual_error = theano.shared(numpy.cast['float32'](7.0))

        for i in xrange(self.n_layers):
            if i == 0:
                input_size = n_ins
            else:
                input_size = hidden_layers_sizes[i - 1]

            if i == 0:
                layer_input = self.x
            else:
                layer_input = self.sigmoid_layers[-1].output

            sigmoid_layer = HiddenLayer(rng=numpy_rng,
                                        input=layer_input,
                                        n_in=input_size,
                                        n_out=hidden_layers_sizes[i],
                                        activation=T.tanh)  ##T.nnet.sigmoid)  # 
            self.sigmoid_layers.append(sigmoid_layer)
            self.params.extend(sigmoid_layer.params)
            self.delta_params.extend(sigmoid_layer.delta_params)

        # add final layer
        if self.output_activation == 'linear':
            self.final_layer = LinearLayer(rng = numpy_rng,
                                           input=self.sigmoid_layers[-1].output,
                                           n_in=hidden_layers_sizes[-1],
                                           n_out=n_outs)
        elif self.output_activation == 'sigmoid':
            self.final_layer = SigmoidLayer(
                 rng = numpy_rng,
                 input=self.sigmoid_layers[-1].output,
                 n_in=hidden_layers_sizes[-1],
                 n_out=n_outs, activation=T.nnet.sigmoid)
        else:
            logger.critical("This output activation function: %s is not supported right now!" %(self.output_activation))
            sys.exit(1)

        self.params.extend(self.final_layer.params)
        self.delta_params.extend(self.final_layer.delta_params)

        ### MSE
#        self.finetune_cost = T.mean(T.sum( (self.final_layer.output-self.y)*(self.final_layer.output-self.y), axis=1 ))
        
        self.errors = T.mean(T.sum( (self.final_layer.output-self.y)*(self.final_layer.output-self.y), axis=1 ))

        ### L1-norm
#        if self.l1_reg is not None:
#            for i in xrange(self.n_layers):
#                W = self.params[i * 2]
#                self.finetune_cost += self.l1_reg * (abs(W).sum())

        ### L2-norm
#        if self.l2_reg is not None:
#            for i in xrange(self.n_layers):
#                W = self.params[i * 2]
#                self.finetune_cost += self.l2_reg * T.sqr(W).sum()


    def build_finetune_functions(self, train_shared_xy, valid_shared_xy, batch_size):

        (train_set_x, train_set_y) = train_shared_xy
        (valid_set_x, valid_set_y) = valid_shared_xy

        # compute number of minibatches for training, validation and testing
        n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
        n_valid_batches /= batch_size

        index = T.lscalar('index')  # index to a [mini]batch
        learning_rate = T.fscalar('learning_rate')
        momentum = T.fscalar('momentum')         
        percetual_error = T.fscalar('percetual_error')
        percepual_scalar = T.fscalar('percepual_scalar')

        self.p_errors = 0.0
    
        ###i know STEP is in the last stream, here is just for fast experiments. will change it later
        perceptual_factor = T.sum( (self.final_layer.output[:, 259:259+55] - self.y[:, 259:259+55])**2, axis=1)
#        perceptual_factor = perceptual_factor / percetual_error #self.percetual_error.get_value() #T.mean(perceptual_error)

        perceptual_factor = perceptual_factor / percetual_error

        perceptual_factor = perceptual_factor ** percepual_scalar

        self.p_errors += T.mean(T.sum( (self.final_layer.output[:, 259:259+55] - self.y[:, 259:259+55])**2, axis=1))

        self.finetune_cost = T.mean(T.sum( (self.final_layer.output-self.y)*(self.final_layer.output-self.y), axis=1 ))

        self.finetune_cost  = 0.0
        self.finetune_cost += T.mean(T.sum( (self.final_layer.output[:, 0:60]  -self.y[:, 0:60])  *(self.final_layer.output[:, 0:60]  -self.y[:, 0:60])  , axis=1 ) * perceptual_factor)
        self.finetune_cost += T.mean(T.sum( (self.final_layer.output[:, 60:314]-self.y[:, 60:314])*(self.final_layer.output[:, 60:314]-self.y[:, 60:314]), axis=1 ))

        ### L1-norm
        if self.l1_reg is not None:
            for i in xrange(self.n_layers):
                W = self.params[i * 2]
                self.finetune_cost += self.l1_reg * (abs(W).sum())

        ### L2-norm
        if self.l2_reg is not None:
            for i in xrange(self.n_layers):
                W = self.params[i * 2]
                self.finetune_cost += self.l2_reg * T.sqr(W).sum()



        layer_size = len(self.params)
        lr_list = []
        for i in xrange(layer_size):
            lr_list.append(learning_rate)

        ##top 2 layers use a smaller learning rate
        if layer_size > 4:
            for i in range(layer_size-4, layer_size):
                lr_list[i] = learning_rate * 0.5

        # compute list of fine-tuning updates
        # compute the gradients with respect to the model parameters
        gparams = T.grad(self.finetune_cost, self.params)

        updates = theano.compat.python2x.OrderedDict()
        layer_index = 0
        for dparam, gparam in zip(self.delta_params, gparams):
            updates[dparam] = momentum * dparam - gparam * lr_list[layer_index]
            layer_index += 1

        for dparam, param in zip(self.delta_params, self.params):
            updates[param] = param + updates[dparam]

#        train_fn = theano.function(inputs=[index, theano.Param(learning_rate, default = 0.0001),
#              theano.Param(momentum, default = 0.5)],
#              outputs=self.errors,
#              updates=updates,
#              givens={self.x: train_set_x[index * batch_size:
#                                          (index + 1) * batch_size],
#                      self.y: train_set_y[index * batch_size:
#                                          (index + 1) * batch_size]})
                                          
        train_fn = theano.function(inputs=[index, theano.Param(learning_rate, default = 0.0001),
              theano.Param(momentum, default = 0.5), theano.Param(percetual_error, default=7.0), theano.Param(percepual_scalar, default=0.0)],
              outputs=self.p_errors,
              updates=updates,
              givens={self.x: train_set_x[index * batch_size:
                                          (index + 1) * batch_size],
                      self.y: train_set_y[index * batch_size:
                                          (index + 1) * batch_size]})

        valid_fn = theano.function([index], 
              outputs=self.errors,
              givens={self.x: valid_set_x[index * batch_size:
                                          (index + 1) * batch_size],
                      self.y: valid_set_y[index * batch_size:
                                          (index + 1) * batch_size]})

        valid_score_i = theano.function([index], 
              outputs=self.errors,
              givens={self.x: valid_set_x[index * batch_size:
                                          (index + 1) * batch_size],
                      self.y: valid_set_y[index * batch_size:
                                          (index + 1) * batch_size]})

        # Create a function that scans the entire validation set
        def valid_score():
            return [valid_score_i(i) for i in xrange(n_valid_batches)]

        return train_fn, valid_fn, valid_score



    def parameter_prediction(self, test_set_x):  #, batch_size

        n_test_set_x = test_set_x.get_value(borrow=True).shape[0]

        test_out = theano.function([], self.final_layer.output,
              givens={self.x: test_set_x[0:n_test_set_x]})

        predict_parameter = test_out()

        return predict_parameter


if __name__ == '__main__':

    train_scp = '/afs/inf.ed.ac.uk/group/project/dnn_tts/data/nick/nn_scp/train.scp'
    valid_scp = '/afs/inf.ed.ac.uk/group/project/dnn_tts/data/nick/nn_scp/gen.scp'
	
    model_dir = '/afs/inf.ed.ac.uk/group/project/dnn_tts/practice/nnets_model'
	
    log_dir =  '/afs/inf.ed.ac.uk/group/project/dnn_tts/practice/log'   

    finetune_lr=0.01
    pretraining_epochs=100
    pretrain_lr=0.01
    training_epochs=100
    batch_size=32

    n_ins = 898
    n_outs = 229
    
    hidden_layers_sizes = [512, 512, 512]
             
#    test_DBN(train_scp, valid_scp, log_dir, model_dir, n_ins, n_outs, hidden_layers_sizes, 
#             finetune_lr, pretraining_epochs, pretrain_lr, training_epochs, batch_size)

    dnn_generation()

