Baseline HTS: /exports/work/inf_hcrc_cstr_general/zhizheng/DNN/hts_nn_nick (Eddie)
Data: http://homepages.inf.ed.ac.uk/zwu2/dnn_baseline/dnn_baseline_practice.tar.gz

0. find a GPU sever, and install theano
1. copy the data to your work directory
2. modify the 'myconfigfile_mvn_DNN_basic.conf' in the data package to fit your working environment
3. modify the './run_dnn.sh' file to link 'run_dnn.py' with your config file.
4. run './run_dnn.sh'. It will lock a GPU automatically. If no GPU is available, the code will NOT run :)
5. if everything goes well, it gives a MCD result, synthesized voice, and reference copy-synthesis voice after 3-5 hours. 

Let me know if you have any questions.
Zhizheng Wu (zhizheng.wu@ed.ac.uk)
