
import  sys, numpy, re, math
from io_funcs.binary_io import BinaryIOCollection

class SilenceRemover(object):
    def __init__(self, n_cmp, silence_pattern=['*-#+*']):
        self.silence_pattern = silence_pattern
        self.silence_pattern_size = len(silence_pattern)
        self.n_cmp = n_cmp
        
    def remove_silence(self, in_data_list, in_align_list, out_data_list):
        file_number = len(in_data_list)
        align_file_number = len(in_align_list)

        if  file_number != align_file_number:
            print   "The number of input and output files does not equal!\n"
            sys.exit(1)
        if  file_number != len(out_data_list):
            print   "The number of input and output files does not equal!\n"
            sys.exit(1)

        io_funcs = BinaryIOCollection()
        for i in xrange(file_number):

            nonsilence_indices = self.load_alignment(in_align_list[i])
            ori_cmp_data = io_funcs.load_binary_file(in_data_list[i], self.n_cmp)
            
            frame_number = ori_cmp_data.size/self.n_cmp
            if len(nonsilence_indices) == frame_number:
                continue

            new_cmp_data = ori_cmp_data[nonsilence_indices,]

            io_funcs.array_to_binary_file(new_cmp_data, out_data_list[i])


    def check_silence_pattern(self, label):
        label_size = len(label)
        binary_flag = 0
        for si in xrange(self.silence_pattern_size):
            current_pattern = self.silence_pattern[si]
            current_size = len(current_pattern)
            if current_pattern[0] == '*' and current_pattern[current_size - 1] == '*':
                temp_pattern = current_pattern[1:current_size - 1]
                for il in xrange(1, label_size - current_size + 2):
                    if temp_pattern == label[il:il + current_size - 2]:
                        binary_flag = 1
            elif current_pattern[current_size-1] != '*':
                temp_pattern = current_pattern[1:current_size]
                if temp_pattern == label[label_size - current_size + 1:label_size]:
                    binary_flag = 1
            elif current_pattern[0] != '*':
                temp_pattern = current_pattern[0:current_size - 1]
                if temp_pattern == label[0:current_size - 1]:
                    binary_flag = 1
            if binary_flag == 1:
                break
        
        return  binary_flag # one means yes, zero means no

    def load_alignment(self, alignment_file_name):

        base_frame_index = 0
        nonsilence_frame_index_list = []
        fid = open(alignment_file_name)
        for line in fid.readlines():
            line = line.strip()
            if len(line) < 1:
                continue
            temp_list = re.split('\s+', line)
            start_time = int(temp_list[0])
            end_time = int(temp_list[1])
            full_label = temp_list[2]
            frame_number = int((end_time - start_time)/50000)

            label_binary_flag = self.check_silence_pattern(full_label)

            if label_binary_flag == 0:
                for frame_index in xrange(frame_number):
                    nonsilence_frame_index_list.append(base_frame_index + frame_index)
            base_frame_index = base_frame_index + frame_number
#            print   start_time, end_time, frame_number, base_frame_index
        fid.close()
        
        return  nonsilence_frame_index_list

#    def load_binary_file(self, file_name, dimension):
        
#        fid_lab = open(file_name, 'rb')
#        features = numpy.fromfile(fid_lab, dtype=numpy.float32)
#        fid_lab.close()
#        features = features[:(dimension * (features.size / dimension))]
#        features = features.reshape((-1, dimension))
        
#        return  features


def trim_silence(in_list, out_list, in_dimension, label_list, label_dimension, \
                                       silence_feature_index, percent_to_keep=0):
    '''
    Function to trim silence from binary label/speech files based on binary labels.
        in_list: list of binary label/speech files to trim
        out_list: trimmed files
        in_dimension: dimension of data to trim
        label_list: list of binary labels which contain trimming criterion
        label_dimesion:
        silence_feature_index: index of feature in labels which is silence: 1 means silence (trim), 0 means leave.
    '''
    assert len(in_list) == len(out_list) == len(label_list)
    io_funcs = BinaryIOCollection()
    for (infile, outfile, label_file) in zip(in_list, out_list, label_list):
        data = io_funcs.load_binary_file(infile, in_dimension)
        label = io_funcs.load_binary_file(label_file, label_dimension)
        assert math.fabs(data.shape[0] - label.shape[0]) < 3,'%s and %s contain different numbers of frames: %s %s'%(infile, label_file,  data.shape[0], label.shape[0])
        silence_flag = label[:, silence_feature_index]
#         print silence_flag
        assert (numpy.unique(silence_flag) == numpy.array([0,1])).all(),'dimension %s of %s contains values other than 0 and 1 (or else all 1s or 0s)'%(silence_feature_index, infile)
        print 'Remove %d%% of frames (%s frames) as silence... '%(100 * numpy.sum(silence_flag / float(len(silence_flag))), int(numpy.sum(silence_flag)))
        non_silence_indices = numpy.nonzero(silence_flag == 0)  ## get the indices where silence_flag == 0 is True (i.e. != 0)
        if percent_to_keep != 0:
            assert type(percent_to_keep) == int and percent_to_keep > 0
            silence_indices = numpy.nonzero(silence_flag == 1)
            ## nonzero returns a tuple of arrays, one for each dimension of input array
            silence_indices = silence_indices[0]
            every_nth = 100  / percent_to_keep
            silence_indices_to_keep = silence_indices[::every_nth]  ## every_nth used +as step value in slice
            if len(silence_indices_to_keep) == 0:
                silence_indices_to_keep = numpy.array([1]) ## avoid errors in case there is no silence
            print '   Restore %s%% (every %sth frame: %s frames) of silent frames'%(percent_to_keep, every_nth, len(silence_indices_to_keep))

            ## Append to end of utt -- same function used for labels and audio
            ## means that violation of temporal order doesn't matter -- will be consistent.
            ## Later, frame shuffling will disperse silent frames evenly across minibatches:
            non_silence_indices = ( numpy.hstack( [non_silence_indices[0], silence_indices_to_keep] ) ) 
                                                    ##  ^---- from tuple and back (see nonzero note above)

        trimmed_data = data[non_silence_indices, :]  ## advanced integer indexing
        io_funcs.array_to_binary_file(trimmed_data, outfile)  
    

if  __name__ == '__main__':
    
    cmp_file_list_name = ''
    lab_file_list_name = ''
    align_file_list_name = ''

    n_cmp = 229
    n_lab = 898

    in_cmp_list = ['/group/project/dnn_tts/data/nick/nn_cmp/nick/herald_001.cmp']
    in_lab_list = ['/group/project/dnn_tts/data/nick/nn_new_lab/herald_001.lab']
    in_align_list = ['/group/project/dnn_tts/data/cassia/nick_lab/herald_001.lab']
    
    out_cmp_list = ['/group/project/dnn_tts/data/nick/nn_new_lab/herald_001.tmp.cmp']
    out_lab_list = ['/group/project/dnn_tts/data/nick/nn_new_lab/herald_001.tmp.no.lab']
    
    remover = SilenceRemover(in_cmp_list, in_align_list, n_cmp, out_cmp_list)
    remover.remove_silence()

