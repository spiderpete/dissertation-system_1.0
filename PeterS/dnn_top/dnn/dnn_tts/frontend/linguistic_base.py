
import logging

## a generic class of linguistic feature extraction
## 
class LinguisticBase(object):
    def __init__(self, dimension=0):
        self.dimension = dimension  ##the feature dimensionality of output (should that read 'input' ?)

        ## the number of utterances to be normalised
        self.utterance_num = 0

    
    ## the ori_file_list contains the file paths of the raw linguistic data
    ## the output_file_list contains the file paths of the normalised linguistic data
    ## 
    def perform_normalisation(self, ori_file_list, output_file_list):
        
        logger = logging.getLogger("perform_normalisation")
        logger.info('perform linguistic feature extraction')
        self.utterance_num = len(ori_file_list)
        if self.utterance_num != len(output_file_list):
            logger.error('the number of input and output linguistic files should be the same!\n')
            sys.exit(1)

        for i in xrange(self.utterance_num):
            self.extract_linguistic_features(ori_file_list[i], output_file_list[i])
    
    ## the exact function to do the work
    ## need to be implemented in the specific class
    ## the function will write the linguistic features directly to the output file
    def extract_linguistic_features(self, in_file_name, out_file_name):
        pass
