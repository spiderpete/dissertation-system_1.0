
#io_funcs.

from io_funcs.binary_io import  BinaryIOCollection
import os, numpy
import logging

from mlpg import MLParameterGeneration

class   ParameterGeneration(object):

    def __init__(self, gen_wav_features = ['mgc', 'lf0', 'bap']):
        self.gen_wav_features = gen_wav_features
        self.inf_float = -1.0e+10

        # not really necessary to have the logger rembered in the class - can easily obtain it by name instead
        # self.logger = logging.getLogger('param_generation')

        self.var = {}
        
    def acoustic_decomposition(self, in_file_list, dimension, out_dimension_dict, file_extension_dict, var_file_dict):

        logger = logging.getLogger('param_generation')

        logger.debug('acoustic_decomposition for %d files' % len(in_file_list) )

        self.load_covariance(var_file_dict, out_dimension_dict)

        stream_start_index = {}
        dimension_index = 0
        recorded_vuv = False
        vuv_dimension = None

        for feature_name in out_dimension_dict.keys():
#            if feature_name != 'vuv':
            stream_start_index[feature_name] = dimension_index
#            else:
#                vuv_dimension = dimension_index
#                recorded_vuv = True
            
            dimension_index += out_dimension_dict[feature_name]

        io_funcs = BinaryIOCollection()

        mlpg_algo = MLParameterGeneration()

        findex=0
        flen=len(in_file_list)
        for file_name in in_file_list:
            
            findex=findex+1
            
            dir_name = os.path.dirname(file_name)
            file_id = os.path.splitext(os.path.basename(file_name))[0]

            features, frame_number = io_funcs.load_binary_file_frame(file_name, dimension)
            
            logger.info('processing %4d of %4d: %s' % (findex,flen,file_name) )

            for feature_name in self.gen_wav_features:
                
                logger.debug(' feature: %s' % feature_name)
                
                current_features = features[:, stream_start_index[feature_name]:stream_start_index[feature_name]+out_dimension_dict[feature_name]]
            
                gen_features = mlpg_algo.generation(current_features, self.var[feature_name], out_dimension_dict[feature_name]/3)

                logger.debug(' feature dimensions: %d by %d' %(gen_features.shape[0], gen_features.shape[1]))

                if feature_name in ['lf0', 'F0']:
                    if stream_start_index.has_key('vuv'):
                        vuv_feature = features[:, stream_start_index['vuv']:stream_start_index['vuv']+1]

                        for i in xrange(frame_number):
                            if vuv_feature[i, 0] < 0.5:
                                gen_features[i, 0] = self.inf_float

                new_file_name = os.path.join(dir_name, file_id + file_extension_dict[feature_name])

                io_funcs.array_to_binary_file(gen_features, new_file_name)
                logger.debug(' wrote to file %s' % new_file_name)


    def load_covariance(self, var_file_dict, out_dimension_dict):

        io_funcs = BinaryIOCollection()
        for feature_name in var_file_dict.keys():
            var_values, dimension = io_funcs.load_binary_file_frame(var_file_dict[feature_name], 1)

            var_values = numpy.reshape(var_values, (out_dimension_dict[feature_name], 1))

            self.var[feature_name] = var_values

if __name__ == '__main__':
    
    in_file_list = ['/afs/inf.ed.ac.uk/group/project/dnn_tts/mtl_dnn/gen/dnn_2500_601_229/hvd_678.cmp']

    out_dimension_dict = { 'mgc' : 150,
                           'lf0' : 3,
                           'vuv' : 1,
                           'bap' : 75}
        
    file_extension_dict = {'mgc' : '.mgc',
                           'lf0' : '.lf0',
                           'vuv' : '.vuv',
                           'bap' : '.bap'}

    var_file_dict  = { 'mgc' : '/afs/inf.ed.ac.uk/group/project/dnn_tts/mtl_dnn/data/var/mgc',
                       'lf0' : '/afs/inf.ed.ac.uk/group/project/dnn_tts/mtl_dnn/data/var/lf0',
                       'bap' : '/afs/inf.ed.ac.uk/group/project/dnn_tts/mtl_dnn/data/var/bap'}
         
    generator = ParameterGeneration()

    generator.acoustic_decomposition(in_file_list, 229, out_dimension_dict, file_extension_dict, var_file_dict)
