
import numpy, theano, random
from io_funcs.binary_io import BinaryIOCollection
import logging

class ListDataProvider(object):

    def __init__(self, x_file_list, y_file_list, n_ins=0, n_outs=0, buffer_size = 500000, shuffle=False):

        self.logger = logging.getLogger("ListDataProvider")

        self.n_ins = n_ins
        self.n_outs = n_outs

        self.buffer_size = buffer_size
            
        #remove potential empty lines and end of line signs


        try:
            assert len(x_file_list) > 0
        except AssertionError:
            self.logger.critical('first list is empty')
            raise

        try:
            assert len(y_file_list) > 0
        except AssertionError:
            self.logger.critical('second list is empty')
            raise

        try:
            assert len(x_file_list) == len(y_file_list)
        except AssertionError:
            self.logger.critical('two lists are of differing lengths: %d versus %d',len(x_file_list),len(y_file_list))
            raise

        self.x_files_list = x_file_list
        self.y_files_list = y_file_list
        
        self.logger.debug('first  list of items from ...%s to ...%s' % (self.x_files_list[0].rjust(20)[-20:],self.x_files_list[-1].rjust(20)[-20:]) )
        self.logger.debug('second list of items from ...%s to ...%s' % (self.y_files_list[0].rjust(20)[-20:],self.y_files_list[-1].rjust(20)[-20:]) )
        
        if shuffle: 
            random.seed(271638)
            random.shuffle(self.x_files_list)
            random.seed(271638)
            random.shuffle(self.y_files_list)

        self.file_index = 0
        self.list_size = len(self.x_files_list)

        self.remain_data_x = numpy.empty((0, self.n_ins))
        self.remain_data_y = numpy.empty((0, self.n_outs))
        self.remain_frame_number = 0

        self.end_reading = False
    
        self.logger.debug('initialised')
    
    def __iter__(self):
        return self 
    
    def reset(self):
        self.file_index = 0
        self.end_reading = False

        self.remain_frame_number = 0

        self.logger.debug('reset')

    def make_shared(self, data_set, data_name):
        data_set = theano.shared(numpy.asarray(data_set, dtype=theano.config.floatX), name=data_name, borrow=True)

        return  data_set

    def load_next_partition(self):
        
        self.logger.debug('loading next partition')
        
        temp_set_x = numpy.empty((self.buffer_size, self.n_ins))
        temp_set_y = numpy.empty((self.buffer_size, self.n_outs))
        current_index = 0

        ### first check whether there are remaining data from previous utterance
        if self.remain_frame_number > 0:
            temp_set_x[current_index:self.remain_frame_number, ] = self.remain_data_x
            temp_set_y[current_index:self.remain_frame_number, ] = self.remain_data_y
            current_index += self.remain_frame_number
            
            self.remain_frame_number = 0
        
        io_fun = BinaryIOCollection()
        while True:
            if current_index >= self.buffer_size:
                break
            if  self.file_index >= self.list_size:
                self.end_reading = True
                self.file_index = 0
                break

            in_features, lab_frame_number = io_fun.load_binary_file_frame(self.x_files_list[self.file_index], self.n_ins)
            out_features, out_frame_number = io_fun.load_binary_file_frame(self.y_files_list[self.file_index], self.n_outs)
            
            frame_number = lab_frame_number
            if abs(lab_frame_number - out_frame_number) < 5:    ## we allow small difference here. may not be correct, but sometimes, there is one/two frames difference
                if lab_frame_number > out_frame_number:
                    frame_number = out_frame_number
            else:
                self.logger.critical("the number of frames in label and acoustic features are different: %d vs %d" %(lab_frame_number, out_frame_number))
                print lab_frame_number
                print out_frame_number
                raise

            out_features = out_features[0:frame_number, ]
            in_features = in_features[0:frame_number, ]

            if current_index + frame_number <= self.buffer_size:   
                temp_set_x[current_index:current_index+frame_number, ] = in_features
                temp_set_y[current_index:current_index+frame_number, ] = out_features
                    
                current_index = current_index + frame_number
            else:   ## if current utterance cannot be stored in the block, then leave the remaining part for the next block
                used_frame_number = self.buffer_size - current_index
                temp_set_x[current_index:self.buffer_size, ] = in_features[0:used_frame_number, ]
                temp_set_y[current_index:self.buffer_size, ] = out_features[0:used_frame_number, ]
                current_index = self.buffer_size

                self.remain_data_x = in_features[used_frame_number:frame_number, ]
                self.remain_data_y = out_features[used_frame_number:frame_number, ]
                self.remain_frame_number = frame_number - used_frame_number

            self.file_index += 1

        temp_set_x = temp_set_x[0:current_index, ]
        temp_set_y = temp_set_y[0:current_index, ]

        numpy.random.seed(271639)
        numpy.random.shuffle(temp_set_x)
        numpy.random.seed(271639)
        numpy.random.shuffle(temp_set_y)

        shared_set_x = self.make_shared(temp_set_x, 'x')
        shared_set_y = self.make_shared(temp_set_y, 'y')

        shared_set_xy = (shared_set_x, shared_set_y)
#        temp_set_x = self.make_shared(temp_set_x, 'x')
#        temp_set_y = self.make_shared(temp_set_y, 'y')

        return shared_set_xy, temp_set_x, temp_set_y
        
    def is_finish(self):
        return self.end_reading


class ListDataProviderWithProjectionIndex(ListDataProvider):
    '''
    Added kwarg index_to_project to __init__
    '''

    def __init__(self, x_file_list, y_file_list, n_ins=0, n_outs=0, \
            buffer_size = 500000, shuffle=False, index_to_project=1, projection_insize=10000, indexes_only=False):
        ##ListDataProvider.__init__(x_file_list, \
        ##         y_file_list, n_ins=0, n_outs=0, buffer_size = 500000, shuffle=False) 
        super( ListDataProviderWithProjectionIndex, self ).__init__(x_file_list, \
                 y_file_list, n_ins=n_ins, n_outs=n_outs, buffer_size=buffer_size, shuffle=shuffle)                               
        self.index_to_project = index_to_project
        self.projection_insize = projection_insize
        self.indexes_only = indexes_only

    def load_next_partition_with_projection(self):
    
        shared_set_xy, temp_set_x, temp_set_y = self.load_next_partition()
        
        if self.indexes_only:
            temp_set_x, p_indexes = get_unexpanded_projection_inputs(temp_set_x, self.index_to_project, \
                                                            self.projection_insize)
            shared_set_x_proj = theano.shared(p_indexes, name='x_proj', borrow=True)                                            
        else:
            temp_set_x, one_hot = expand_projection_inputs(temp_set_x, self.index_to_project, \
                                                            self.projection_insize)                                                                        
            shared_set_x_proj = self.make_shared(one_hot, 'x_proj')
            
        shared_set_x = self.make_shared(temp_set_x, 'x')
        shared_set_y = self.make_shared(temp_set_y, 'y')

        shared_set_xy = (shared_set_x, shared_set_x_proj, shared_set_y)

        if self.indexes_only:
            return shared_set_xy, temp_set_x, p_indexes, temp_set_y
        else:
            return shared_set_xy, temp_set_x, one_hot, temp_set_y
            
## Put this function at global level so it can be imported for use in dnn_generation
def expand_projection_inputs(temp_set_x, index_to_project, projection_insize):
    ## Turn indexes to words, syllables etc. to one-hot data:
    m,n = numpy.shape(temp_set_x)
    projection_indices = temp_set_x[:, index_to_project]
    #print projection_indices.tolist()
    assert projection_indices.max() < projection_insize,'projection_insize is %s but there is an index %s in the data'%(projection_insize, projection_indices.max())
    one_hot = numpy.zeros((m, projection_insize))
    
    ## Used advanced indexing to turn the relevant features on:
    projection_indices = projection_indices.astype(int) ## check conversion???!?!?!
    #     print projection_indices.tolist()
    #     print '            ^--- proj indices'
    #     print 
    one_hot[range(m),projection_indices] = 1.0
    ## Effectively remove the index from the original data by setting to 0:
    temp_set_x[:, index_to_project] = 0.0
    return temp_set_x, one_hot
    
def get_unexpanded_projection_inputs(temp_set_x, index_to_project, projection_insize):
    ## Turn indexes to words, syllables etc. to one-hot data:
    m,n = numpy.shape(temp_set_x)
    projection_indices = temp_set_x[:, index_to_project]
    #print projection_indices.tolist()
    assert projection_indices.max() < projection_insize,'projection_insize is %s but there is an index %s in the data'%(projection_insize, projection_indices.max())
    
    projection_indices = projection_indices.astype('int32') ## check conversion???!?!?!
    
    temp_set_x[:, index_to_project] = 0.0
    return temp_set_x, projection_indices
    